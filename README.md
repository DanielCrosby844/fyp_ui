# Final Year Project - User Interface

The user interface for my final year project built using create-react-app.

## Other Repositories

- API: https://bitbucket.org/DanielCrosby844/fyp_api
- Build: https://bitbucket.org/DanielCrosby844/fyp_build

## Run

```console
npm install 
npm start
```
