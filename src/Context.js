import { createContext, useReducer } from "react";
import Api from "./Api";

const Context = createContext();

export const ContextProvider = ({ children }) => {
  const initialState = {
    theme: localStorage.getItem("theme") || "dark",
    token: localStorage.getItem("token"),
    movies: [],
    tvshows: [],
    seasons: [],
    episodes: [],
    music: [],
    images: [],
    username: localStorage.getItem("username") || "",
  };
  const [state, setContext] = useReducer(
    (state, newValue) => ({ ...state, ...newValue }),
    initialState
  );
  const refresh = async (collection, prefix = "") => {
    setContext({
      [collection]: await new Api(
        `${prefix}${collection}`,
        state.token
      ).getAll(),
    });
  };
  return (
    <Context.Provider value={{ ...state, setContext, refresh }}>
      {children}
    </Context.Provider>
  );
};

export default Context;
