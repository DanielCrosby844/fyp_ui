import superagent from "superagent";

class Api {
  constructor(collection, token) {
    this.url = `/${collection}/`;
    this.token = token;
  }
  async getAll() {
    return (await superagent.get(this.url).set("Authorization", this.token))
      .body;
  }
  async create(newDocument) {
    await superagent
      .post(`${this.url}`)
      .set("Authorization", this.token)
      .send(newDocument);
  }
  async update(updatedDocument) {
    await superagent
      .put(`${this.url}`)
      .set("Authorization", this.token)
      .send(updatedDocument);
  }
  async delete(deleteDocument) {
    await superagent
      .delete(`${this.url}`)
      .set("Authorization", this.token)
      .send(deleteDocument);
  }
  async uploadFile(file, onProgress) {
    const formData = new FormData();
    formData.append(0, file);
    await superagent
      .post("/file/")
      .set("Authorization", this.token)
      .on("progress", (e) => {
        onProgress(e.percent);
      })
      .send(formData);
  }
}

export default Api;
