import { BrowserRouter } from "react-router-dom";
import Sidebar from "./components/Sidebar";
import Routes from "./Routes";
import Theme from "./components/Theme";
import "./styles/App.scss";

const App = () => {
  return (
    <BrowserRouter basename="/app">
      <Theme />
      <div id="app-container">
        <Sidebar />
        <div id="main-content">
          <Routes />
        </div>
      </div>
    </BrowserRouter>
  );
};

export default App;
