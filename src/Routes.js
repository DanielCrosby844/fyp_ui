import { useContext } from "react";
import { Switch, Route } from "react-router-dom";
import Context from "./Context";
import Login from "./pages/Login";
import Home from "./pages/Home";
import Movies from "./pages/Movies";
import MoviePlayer from "./pages/MoviePlayer";
import TVShows from "./pages/TVShows";
import Seasons from "./pages/Seasons";
import Episodes from "./pages/Episodes";
import TVShowPlayer from "./pages/TVShowPlayer";
import Music from "./pages/Music";
import Images from "./pages/Images";
import Settings from "./pages/Settings";

const Routes = () => {
  const { token } = useContext(Context);
  return (
    <Switch>
      {/* PUBLIC ROUTES */}
      {/* <Route exact path="/login" comnponent={}/> */}
      {/* PRIVATE ROUTES */}
      <Route path="/">
        {!token ? (
          <Login />
        ) : (
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/movies" component={Movies} />
            <Route exact path="/movies/:id" component={MoviePlayer} />
            <Route exact path="/tvshows" component={TVShows} />
            <Route exact path="/tvshows/:id" component={Seasons} />
            <Route
              exact
              path="/tvshows/:id/:seasonNumber"
              component={Episodes}
            />
            <Route
              exact
              path="/tvshows/:id/:seasonNumber/:episodeNumber"
              component={TVShowPlayer}
            />
            <Route exact path="/music" component={Music} />
            <Route exact path="/Images" component={Images} />
            <Route exact path="/settings" component={Settings} />
          </Switch>
        )}
      </Route>
    </Switch>
  );
};

export default Routes;
