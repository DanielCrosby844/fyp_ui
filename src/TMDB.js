import superagent from "superagent";

class TMDB {
  async getConfig() {
    return (
      await superagent
        .get("https://api.themoviedb.org/3/configuration")
        .query(`api_key=${process.env.REACT_APP_TMDB}`)
    ).body;
  }
  async searchMovies(query) {
    const response = await superagent
      .get("https://api.themoviedb.org/3/search/movie")
      .query(`api_key=${process.env.REACT_APP_TMDB}`)
      .query(`query=${query}`);
    return JSON.parse(response.text);
  }
  async searchTVShows(query) {
    const response = await superagent
      .get("https://api.themoviedb.org/3/search/tv")
      .query(`api_key=${process.env.REACT_APP_TMDB}`)
      .query(`query=${query}`);
    return JSON.parse(response.text);
  }
  async getTVShowByID(id) {
    const response = await superagent
      .get(`https://api.themoviedb.org/3/tv/${id}`)
      .query(`api_key=${process.env.REACT_APP_TMDB}`);
    return JSON.parse(response.text);
  }
  async getEpisode(tvshowID, seasonNumber, episodeNumber) {
    const response = await superagent
      .get(
        `https://api.themoviedb.org/3/tv/${tvshowID}/season/${seasonNumber}/episode/${episodeNumber}`
      )
      .query(`api_key=${process.env.REACT_APP_TMDB}`);
    return JSON.parse(response.text);
  }
}

export default new TMDB();
