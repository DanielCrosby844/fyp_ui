import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import MediaCard from "../components/MediaCard";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import EditMovieModal from "../components/modals/EditMovieModal";

const MoviePlayer = ({ match, history }) => {
  const { token, movies, refresh } = useContext(Context);
  const [state, setState] = useState({ movie: { src: "" }, show: false });
  useEffect(async () => {
    if (!movies.length) {
      refresh("movies");
      return;
    }
    const movie = movies.find((movie) => movie._id.$oid === match.params.id);
    if (!movie) {
      return;
    }
    movie.src = `/file/stream?file=${movie.fileName}&Authorization=${token}`;
    setState({ ...state, movie: movie });
  }, [movies]);
  return (
    <div className="page media-player-container">
      <Navbar sticky="top" id="movie-edit-nav">
        <Button
          variant="outline-primary"
          className="ml-auto primary-button"
          onClick={() => setState({ ...state, show: true })}
        >
          Edit
        </Button>
      </Navbar>
      <video
        src={state.movie.src}
        className="media-player"
        controls
        autoPlay
        style={{ minWidth: "60vw" }}
      />
      <MediaCard media={state.movie} />
      <EditMovieModal
        show={state.show}
        movie={state.movie}
        hideModal={() => setState({ ...state, show: false })}
        history={history}
      />
    </div>
  );
};

export default MoviePlayer;
