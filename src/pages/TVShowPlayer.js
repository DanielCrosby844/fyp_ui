import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import MediaCard from "../components/MediaCard";
import Button from "react-bootstrap/Button";

const TVShowPlayer = ({ match, history }) => {
  const { token, tvshows, episodes, refresh } = useContext(Context);
  const [state, setState] = useState({ episode: { src: "" } });
  useEffect(async () => {
    if (!episodes.length || !tvshows.length) {
      refresh("tvshows");
      refresh("episodes", "tvshows/");
      return;
    }
    const { title, imageURL } = tvshows.find(
      (tvshows) => tvshows._id.$oid === match.params.id
    );
    const episode = episodes.find(
      (episode) =>
        episode.tvshow === title &&
        episode.seasonNumber === parseInt(match.params.seasonNumber) &&
        episode.episodeNumber === parseInt(match.params.episodeNumber)
    );
    episode.src = `/file/stream?file=${episode.fileName}&Authorization=${token}`;
    setState({ ...state, episode: { ...episode, imageURL } });
  }, [episodes, match.params.episodeNumber]);
  return (
    <div className="page media-player-container">
      <video
        src={state.episode.src}
        className="media-player"
        controls
        autoPlay
      />
      <div id="episode-numbers">
        {episodes
          .filter(
            (episode) =>
              episode.tvshow === state.episode.tvshow &&
              episode.seasonNumber === parseInt(match.params.seasonNumber)
          )
          .map((episode, index) => {
            return (
              <Button
                key={index}
                disabled={episode.fileName === ""}
                className="episode-number"
                variant="outline-primary"
                onClick={() =>
                  history.push(
                    `/tvshows/${match.params.id}/${match.params.seasonNumber}/${episode.episodeNumber}`
                  )
                }
              >
                {episode.episodeNumber}
              </Button>
            );
          })}
      </div>
      <MediaCard media={state.episode} />
    </div>
  );
};

export default TVShowPlayer;
