import { useContext, useState } from "react";
import Context from "../Context";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import superagent from "superagent";
import { AiOutlineLock } from "react-icons/ai";

const Login = () => {
  const { setContext } = useContext(Context);
  const [state, setState] = useState({ username: "", password: "" });
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await superagent.post("/accounts/login").send(state);
      const token = response.header.authorization;
      localStorage.setItem("token", token);
      localStorage.setItem("username", state.username);
      setContext({ token: token, username: state.username });
    } catch {
      alert("Login Falied");
    }
  };
  return (
    <Container id="login-container" fluid>
      <Form onSubmit={handleSubmit} id="login-form">
        <div id="login-icon">
          <AiOutlineLock />
        </div>
        <Form.Group>
          <Form.Control
            type="text"
            placeholder="Username"
            className="form-input"
            required
            onChange={(e) => setState({ ...state, username: e.target.value })}
          />
        </Form.Group>
        <Form.Group>
          <Form.Control
            type="password"
            placeholder="Password"
            className="form-input"
            required
            onChange={(e) => setState({ ...state, password: e.target.value })}
          />
        </Form.Group>
        <Form.Group>
          <Button type="submit" id="login-button" className="btn-block">
            Log In
          </Button>
        </Form.Group>
      </Form>
    </Container>
  );
};

export default Login;
