import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import TopNav from "../components/TopNav";
import MusicPlayer from "../components/MusicPlayer";
import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/Container";
import NewMusicModal from "../components/modals/NewMusicModal";
import { FiEdit } from "react-icons/fi";
import { AiOutlinePlayCircle } from "react-icons/ai";
import EditMusicModal from "../components/modals/EditMusicModal";

const Music = () => {
  const { token, music, refresh } = useContext(Context);
  const [state, setState] = useState({
    filter: "",
    nowPlaying: -1,
    musicToEdit: {},
    showNewMusicModal: false,
    showEditMusicModal: false,
  });
  useEffect(async () => {
    if (!music.length) {
      refresh("music");
    }
  }, []);
  return (
    <div className="page">
      <TopNav
        onSearch={(e) => setState({ ...state, filter: e.target.value })}
        onAdd={() => setState({ ...state, showNewMusicModal: true })}
      />
      <Container id="music-list" fluid>
        <Table id="music-table">
          <thead>
            <tr>
              <th>Play</th>
              <th>Title</th>
              <th>Artist</th>
              <th>File Name</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            {music
              .filter((track) =>
                track.title.toLowerCase().includes(state.filter.toLowerCase())
              )
              .map((track, index) => {
                return (
                  <tr key={index} className="music-row">
                    <td>
                      <AiOutlinePlayCircle
                        id="play-icon"
                        size={25}
                        onClick={() =>
                          setState({
                            ...state,
                            nowPlaying: index,
                          })
                        }
                      />
                    </td>
                    <td>{track.title}</td>
                    <td>{track.artist}</td>
                    <td>{track.fileName}</td>
                    <td>
                      <FiEdit
                        id="edit-icon"
                        size={20}
                        onClick={() =>
                          setState({
                            ...state,
                            musicToEdit: track,
                            showEditMusicModal: true,
                          })
                        }
                      />
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      </Container>
      <MusicPlayer
        source={
          music.length > 0 && state.nowPlaying >= 0
            ? `/file/stream?file=${
                music[state.nowPlaying].fileName
              }&Authorization=${token}`
            : ""
        }
        onPrevious={() => {
          if (state.nowPlaying === 0) {
            setState({ ...state, nowPlaying: music.length - 1 });
          } else {
            setState({ ...state, nowPlaying: state.nowPlaying - 1 });
          }
        }}
        onNext={() => {
          if (state.nowPlaying === music.length - 1) {
            setState({ ...state, nowPlaying: 0 });
          } else {
            setState({ ...state, nowPlaying: state.nowPlaying + 1 });
          }
        }}
        onEnded={() => {
          if (state.nowPlaying === 0) {
            setState({ ...state, nowPlaying: music.length - 1 });
          } else {
            setState({ ...state, nowPlaying: state.nowPlaying - 1 });
          }
        }}
      />
      <NewMusicModal
        show={state.showNewMusicModal}
        hideModal={() => setState({ ...state, showNewMusicModal: false })}
      />
      <EditMusicModal
        show={state.showEditMusicModal}
        hideModal={() => setState({ ...state, showEditMusicModal: false })}
        music={state.musicToEdit}
      />
    </div>
  );
};

export default Music;
