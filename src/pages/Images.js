import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import Image from "react-bootstrap/Image";
import NewImageModal from "../components/modals/NewImageModal";
import FormControl from "react-bootstrap/FormControl";
import ImageModal from "../components/modals/ImageModal";
import Navbar from "react-bootstrap/Navbar";
import Api from "../Api";
import EditImageModal from "../components/modals/EditImageModal";
import Button from "react-bootstrap/Button";

const Images = () => {
  const { token, images, refresh } = useContext(Context);
  const [state, setState] = useState({
    filter: "",
    showNewImageModal: false,
    showImageModal: false,
    showEditImageModal: false,
    index: 0,
  });
  useEffect(async () => {
    if (!images.length) {
      refresh("images");
    }
  }, []);
  const IMAGES = new Api("images", token);
  return (
    <div className="page">
      <Navbar sticky="top" id="top-nav">
        <FormControl
          type="text"
          placeholder="Search"
          id="nav-search"
          onChange={(e) => setState({ ...state, filter: e.target.value })}
        />
        <div className="ml-auto">
          <Button
            variant="outline-primary"
            className="primary-button"
            style={{ marginRight: "5px" }}
            onClick={() => setState({ ...state, showEditImageModal: true })}
          >
            Edit
          </Button>
          <Button
            variant="outline-primary"
            className="primary-button"
            onClick={() => setState({ ...state, showNewImageModal: true })}
          >
            Add
          </Button>
        </div>
      </Navbar>

      <div className="media-list">
        {images
          .filter((image) =>
            image.fileName.toLowerCase().includes(state.filter.toLowerCase())
          )
          .map((image, index) => {
            return (
              <Image
                key={index}
                className="image"
                src={`/file/download?file=${image.fileName}&Authorization=${token}`}
                onClick={() =>
                  setState({ ...state, index, showImageModal: true })
                }
              />
            );
          })}
      </div>
      <NewImageModal
        show={state.showNewImageModal}
        hideModal={() => setState({ ...state, showNewImageModal: false })}
      />
      <ImageModal
        show={state.showImageModal}
        hideModal={() => setState({ ...state, showImageModal: false })}
      />
      <EditImageModal
        show={state.showEditImageModal}
        hideModal={() => setState({ ...state, showEditImageModal: false })}
      />
    </div>
  );
};

export default Images;
