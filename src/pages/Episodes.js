import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import Api from "../Api";
import TopNav from "../components/TopNav";
import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/Container";
import NewEpisodeModal from "../components/modals/NewEpisodeModal";
import EpisodeModal from "../components/modals/EpisodeModal";
import { BsFillTrashFill } from "react-icons/bs";

const Episodes = ({ match, history }) => {
  const { token, tvshows, episodes, refresh } = useContext(Context);
  const [state, setState] = useState({
    episodes: [],
    filter: "",
    showNewEpisodeModal: false,
    showEpisodeModal: false,
    selectedEpisode: {},
    tvshowImage: "",
  });
  const EPISODES = new Api("tvshows/episodes", token);
  useEffect(async () => {
    if (!episodes.length || !tvshows.length) {
      refresh("tvshows");
      refresh("episodes", "tvshows/");
      return;
    }
  }, []);
  useEffect(async () => {
    if (!episodes.length || !tvshows.length) {
      return;
    }
    const { title, imageURL } = tvshows.find(
      (tvshow) => tvshow._id.$oid === match.params.id
    );
    setState({
      ...state,
      episodes: episodes.filter(
        (episode) =>
          episode.tvshow === title &&
          episode.seasonNumber === parseInt(match.params.seasonNumber)
      ),
      tvshowImage: imageURL,
    });
  }, [episodes]);
  return (
    <div className="page">
      <TopNav
        onSearch={(e) => setState({ ...state, filter: e.target.value })}
        onAdd={() => setState({ ...state, showNewEpisodeModal: true })}
      />
      <Container className="episode-list" fluid>
        <Table id="episode-table">
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>File Name</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {state.episodes
              .filter((episode) =>
                episode.title.toLowerCase().includes(state.filter.toLowerCase())
              )
              .map((episode) => {
                return (
                  <tr
                    key={episode.episodeNumber}
                    className="episode-row"
                    style={{ opacity: episode.fileName !== "" ? 1 : 0.3 }}
                    onClick={() => {
                      setState({
                        ...state,
                        selectedEpisode: {
                          ...episode,
                          imageURL: state.tvshowImage,
                        },
                        showEpisodeModal: true,
                      });
                    }}
                  >
                    <td>{episode.episodeNumber}</td>
                    <td>{episode.title}</td>
                    <td>{episode.fileName}</td>
                    <td>
                      <BsFillTrashFill
                        size={20}
                        onClick={async (e) => {
                          e.stopPropagation();
                          await EPISODES.delete({ id: episode._id.$oid });
                          await refresh("episodes", "tvshows/");
                        }}
                      />
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      </Container>
      <NewEpisodeModal
        show={state.showNewEpisodeModal}
        hideModal={() => setState({ ...state, showNewEpisodeModal: false })}
        match={match}
      />
      <EpisodeModal
        show={state.showEpisodeModal}
        hideModal={() => setState({ ...state, showEpisodeModal: false })}
        episode={state.selectedEpisode}
        history={history}
      />
    </div>
  );
};

export default Episodes;
