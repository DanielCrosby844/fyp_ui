import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import MediaCard from "../components/MediaCard";
import NewSeasonModal from "../components/modals/NewSeasonModal";
import Navbar from "react-bootstrap/Navbar";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import EditSeasonModal from "../components/modals/EditSeasonModal";

const Seasons = ({ match, history }) => {
  const { tvshows, seasons, refresh } = useContext(Context);
  const [state, setState] = useState({
    tvshow: "",
    filter: "",
    showModal: false,
    showEditSeasonModal: false,
  });
  useEffect(async () => {
    await refresh("tvshows");
    await refresh("seasons", "tvshows/");
  }, []);
  useEffect(async () => {
    if (!seasons.length || !tvshows.length) {
      return;
    }
    const { title } = tvshows.find(
      (tvshow) => tvshow._id.$oid === match.params.id
    );
    setState({
      ...state,
      tvshow: title,
    });
  }, [seasons]);
  return (
    <div className="page">
      <Navbar sticky="top" id="top-nav">
        <FormControl
          type="text"
          placeholder="Search"
          id="nav-search"
          onChange={(e) => setState({ ...state, filter: e.target.value })}
        />
        <div className="ml-auto">
          <Button
            style={{ marginRight: "5px" }}
            variant="outline-primary"
            className="primary-button"
            onClick={() => setState({ ...state, showEditSeasonModal: true })}
          >
            Edit
          </Button>
          <Button
            variant="outline-primary"
            className="primary-button"
            onClick={() => setState({ ...state, showModal: true })}
          >
            Add
          </Button>
        </div>
      </Navbar>
      <div className="media-list">
        {seasons
          .filter(
            (season) =>
              season.title.toLowerCase().includes(state.filter.toLowerCase()) &&
              season.tvshow === state.tvshow
          )
          .map((season, index) => {
            return (
              <div
                onClick={() =>
                  history.push(
                    `/tvshows/${match.params.id}/${season.seasonNumber}`
                  )
                }
                style={{ marginBottom: "10px" }}
                key={index}
              >
                <MediaCard media={season} />
              </div>
            );
          })}
      </div>
      <NewSeasonModal
        show={state.showModal}
        hideModal={() => setState({ ...state, showModal: false })}
        match={match}
      />
      <EditSeasonModal
        show={state.showEditSeasonModal}
        hideModal={() => setState({ ...state, showEditSeasonModal: false })}
        tvshow={state.tvshow}
      />
    </div>
  );
};

export default Seasons;
