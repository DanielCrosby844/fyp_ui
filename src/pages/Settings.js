import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import { FiEdit } from "react-icons/fi";
import Api from "../Api";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import NewAccountModal from "../components/modals/NewAccountModal";
import EditAccountModal from "../components/modals/EditAccountModal";

const Settings = () => {
  const { theme, token, setContext } = useContext(Context);
  const [state, setState] = useState({
    accounts: [],
    showNewAccountModal: false,
    showEditAccountModal: false,
    accountToEdit: {},
  });
  const ACCOUNTS = new Api("accounts", token);
  useEffect(async () => {
    setState({ ...state, accounts: await ACCOUNTS.getAll() });
  }, []);
  return (
    <div className="page">
      <Navbar sticky="top" id="general-bar">
        <Navbar.Brand className="brand">General</Navbar.Brand>
      </Navbar>
      <Container id="general-container" fluid>
        <Form.Check
          label="Light theme"
          checked={theme === "light"}
          onChange={(e) => {
            if (e.target.checked) {
              localStorage.setItem("theme", "light");
              setContext({ theme: "light" });
            } else {
              localStorage.setItem("theme", "dark");
              setContext({ theme: "dark" });
            }
          }}
        />
      </Container>
      <Navbar id="accounts-bar">
        <Navbar.Brand className="brand">Accounts</Navbar.Brand>
        <Button
          variant="outline-primary"
          className="ml-auto primary-button"
          onClick={() => setState({ ...state, showNewAccountModal: true })}
        >
          Add
        </Button>
      </Navbar>
      <Container fluid id="accounts-container">
        <Table id="accounts-table">
          <thead>
            <tr>
              <th>Edit</th>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {state.accounts.map((account, index) => {
              return (
                <tr key={index}>
                  <td>
                    <FiEdit
                      size={20}
                      className="account-edit"
                      onClick={() => {
                        setState({
                          ...state,
                          showEditAccountModal: true,
                          accountToEdit: account,
                        });
                      }}
                    />
                  </td>
                  <td>{account.username}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Container>
      <NewAccountModal
        show={state.showNewAccountModal}
        setAccounts={(accounts) =>
          setState({ ...state, accounts: accounts, showNewAccountModal: false })
        }
      />
      <EditAccountModal
        show={state.showEditAccountModal}
        account={state.accountToEdit}
        hideModal={() => setState({ ...state, showEditAccountModal: false })}
        setAccounts={(accounts) =>
          setState({
            ...state,
            showEditAccountModal: false,
            accounts: accounts,
          })
        }
      />
    </div>
  );
};

export default Settings;
