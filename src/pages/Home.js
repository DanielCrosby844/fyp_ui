import { Link } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import Context from "../Context";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import CountUp from "react-countup";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Jumbotron from "react-bootstrap/Jumbotron";
import { BiCameraMovie, BiMusic } from "react-icons/bi";
import { CgScreen } from "react-icons/cg";
import { BsImages } from "react-icons/bs";

const Home = ({ history }) => {
  const {
    username,
    movies,
    tvshows,
    music,
    images,
    refresh,
    setContext,
  } = useContext(Context);
  useEffect(async () => {
    if (!movies.length || !tvshows.length || !music.length || !images.length) {
      refresh("movies");
      refresh("tvshows");
      refresh("music");
      refresh("images");
    }
  }, []);
  return (
    <div className="page" id="home-container">
      <Navbar sticky="top" id="home-nav">
        <Navbar.Brand id="welcome-brand">Welcome {username}</Navbar.Brand>
        <Button
          className="ml-auto primary-button"
          onClick={() => {
            localStorage.removeItem("token");
            localStorage.removeItem("username");
            setContext({ token: null, username: "" });
          }}
        >
          Log Out
        </Button>
      </Navbar>
      <Container fluid>
        <Jumbotron id="banner">
          <h1 style={{ textAlign: "center", fontSize: "60px" }}>
            Welcome {username}
          </h1>
          <p></p>
        </Jumbotron>
        <Row>
          <Col lg="3" md="6">
            <Card className="info-card" onClick={() => history.push("/movies")}>
              <Card.Header>
                <BiCameraMovie size={20} />
              </Card.Header>
              <Card.Body>
                <Card.Title>
                  Movies: <CountUp end={movies.length} />
                </Card.Title>
              </Card.Body>
              <Card.Footer></Card.Footer>
            </Card>
          </Col>
          <Col lg="3" md="6">
            <Card
              className="info-card"
              onClick={() => history.push("/tvshows")}
            >
              <Card.Header>
                <CgScreen size={20} />
              </Card.Header>
              <Card.Body>
                <Card.Title>
                  TVShows: <CountUp end={tvshows.length} />
                </Card.Title>
              </Card.Body>
              <Card.Footer></Card.Footer>
            </Card>
          </Col>
          <Col lg="3" md="6">
            <Card className="info-card" onClick={() => history.push("/music")}>
              <Card.Header>
                <BiMusic size={20} />
              </Card.Header>
              <Card.Body>
                <Card.Title>
                  Music: <CountUp end={music.length} />
                </Card.Title>
              </Card.Body>
              <Card.Footer></Card.Footer>
            </Card>
          </Col>
          <Col lg="3" md="6">
            <Card className="info-card" onClick={() => history.push("/images")}>
              <Card.Header>
                <BsImages size={20} />
              </Card.Header>
              <Card.Body>
                <Card.Title>
                  Images: <CountUp end={images.length} />
                </Card.Title>
              </Card.Body>
              <Card.Footer></Card.Footer>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Home;
