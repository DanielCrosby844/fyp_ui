import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import Poster from "../components/Poster";
import NewTVShowModal from "../components/modals/NewTVShowModal";
import EditTVShowModal from "../components/modals/EditTVShowModal";
import Navbar from "react-bootstrap/Navbar";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";

const TVShows = () => {
  const { tvshows, refresh } = useContext(Context);
  const [state, setState] = useState({
    filter: "",
    showModal: false,
    showEditTVShowModal: false,
  });
  useEffect(async () => {
    if (!tvshows.length) {
      refresh("tvshows");
    }
  }, []);
  return (
    <div className="page">
      <Navbar sticky="top" id="top-nav">
        <FormControl
          type="text"
          placeholder="Search"
          id="nav-search"
          onChange={(e) => setState({ ...state, filter: e.target.value })}
        />
        <div className="ml-auto">
          <Button
            style={{ marginRight: "5px" }}
            variant="outline-primary"
            className="primary-button"
            onClick={() => setState({ ...state, showEditTVShowModal: true })}
          >
            Edit
          </Button>
          <Button
            variant="outline-primary"
            className="primary-button"
            onClick={() => setState({ ...state, showModal: true })}
          >
            Add
          </Button>
        </div>
      </Navbar>
      <div className="media-list">
        {tvshows
          .filter((tvshow) =>
            tvshow.title.toLowerCase().includes(state.filter.toLowerCase())
          )
          .map((tvshow, index) => {
            return (
              <Poster
                key={index}
                to={`/tvshows/${tvshow._id.$oid}`}
                imageURL={tvshow.imageURL}
              />
            );
          })}
      </div>
      <NewTVShowModal
        show={state.showModal}
        hideModal={() => setState({ ...state, showModal: false })}
      />
      <EditTVShowModal
        show={state.showEditTVShowModal}
        hideModal={() => setState({ ...state, showEditTVShowModal: false })}
      />
    </div>
  );
};

export default TVShows;
