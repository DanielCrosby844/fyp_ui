import { useEffect, useContext, useState } from "react";
import Context from "../Context";
import TopNav from "../components/TopNav";
import Poster from "../components/Poster";
import NewMovieModal from "../components/modals/NewMovieModal";

const Movies = () => {
  const { movies, refresh } = useContext(Context);
  const [state, setState] = useState({ filter: "", showModal: false });
  useEffect(async () => {
    if (!movies.length) {
      refresh("movies");
    }
  }, []);
  return (
    <div className="page">
      <TopNav
        onSearch={(e) => setState({ ...state, filter: e.target.value })}
        onAdd={() => setState({ ...state, showModal: true })}
      />
      <div className="media-list">
        {movies
          .filter((movie) =>
            movie.title.toLowerCase().includes(state.filter.toLowerCase())
          )
          .map((movie, index) => {
            return (
              <Poster
                key={index}
                to={`/movies/${movie._id.$oid}`}
                imageURL={movie.imageURL}
              />
            );
          })}
      </div>
      <NewMovieModal
        show={state.showModal}
        hideModal={() => setState({ ...state, showModal: false })}
      />
    </div>
  );
};

export default Movies;
