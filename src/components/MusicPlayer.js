import Navbar from "react-bootstrap/Navbar";
import AudioPlayer from "react-h5-audio-player";

const MusicPlayer = ({ source, onPrevious, onNext, onEnded }) => {
  return (
    <Navbar id="music-player-nav">
      <AudioPlayer
        src={source}
        autoPlay
        showSkipControls
        onClickPrevious={onPrevious}
        onClickNext={onNext}
        onEnded={onEnded}
      />
    </Navbar>
  );
};

export default MusicPlayer;
