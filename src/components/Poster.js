import { Link } from "react-router-dom";

const Poster = ({ to, imageURL }) => {
  return (
    <Link to={to}>
      <img src={imageURL} className="media-poster" />
    </Link>
  );
};

export default Poster;
