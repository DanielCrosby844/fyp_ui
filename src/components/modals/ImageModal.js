import { useContext } from "react";
import Context from "../../Context";
import Modal from "react-bootstrap/Modal";
import ImageGallery from "react-image-gallery";

const ImageModal = ({ show, hideModal }) => {
  const { token, images } = useContext(Context);
  if (!images.length) {
    return null;
  }
  return (
    <Modal
      show={show}
      style={{ backgroundColor: "black", textAlign: "center" }}
      centered
      size="lg"
      onHide={hideModal}
    >
      <Modal.Body style={{ backgroundColor: "black" }}>
        <ImageGallery
          showThumbnails={false}
          items={images.map((image) => {
            return {
              original: `/file/download?file=${image.fileName}&Authorization=${token}`,
            };
          })}
        />
      </Modal.Body>
    </Modal>
  );
};

export default ImageModal;
