import { useState, useContext } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import ListGroup from "react-bootstrap/ListGroup";
import TMDB from "../../TMDB";
import Button from "react-bootstrap/Button";
import ProgressBar from "react-bootstrap/ProgressBar";
import Api from "../../Api";

const NewTVShowModal = ({ show, hideModal }) => {
  const { token, setContext, refresh } = useContext(Context);
  const initialState = {
    searchResults: [],
    selectedIndex: -1,
    showCustomForm: false,
    newTVShow: {},
    showProgress: false,
    percentage: 0,
  };
  const [state, setState] = useState(initialState);
  const TVSHOWS = new Api("tvshows", token);
  const SEASONS = new Api("tvshows/seasons", token);
  const EPISODES = new Api("tvshows/episodes", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Add TVShow</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {state.showProgress ? (
          <ProgressBar
            animated
            now={state.percentage}
            label={`${Math.trunc(state.percentage)}%`}
          />
        ) : (
          <Form>
            <Form.Group>
              <Form.Control
                placeholder="Search..."
                type="search"
                disabled={state.showCustomForm}
                onChange={async (e) => {
                  setState({
                    ...state,
                    searchResults:
                      e.target.value === ""
                        ? []
                        : (await TMDB.searchTVShows(e.target.value)).results,
                  });
                }}
              />
            </Form.Group>
            <ListGroup className="search-results">
              {state.searchResults.map((result, index) => {
                return (
                  <ListGroup.Item
                    key={index}
                    active={index === state.selectedIndex}
                    onClick={() => setState({ ...state, selectedIndex: index })}
                  >
                    {result.name}
                  </ListGroup.Item>
                );
              })}
            </ListGroup>
            <Form.Group>
              <Form.Check
                label="Custom"
                onChange={(e) => {
                  const checked = e.target.checked;
                  setState({
                    ...state,
                    searchResults: [],
                    showCustomForm: checked,
                  });
                }}
              />
            </Form.Group>
            {state.showCustomForm && (
              <span>
                <Form.Group>
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) =>
                      setState({
                        ...state,
                        newTVShow: {
                          ...state.newTVShow,
                          title: e.target.value,
                        },
                      })
                    }
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    onChange={(e) =>
                      setState({
                        ...state,
                        newTVShow: {
                          ...state.newTVShow,
                          description: e.target.value,
                        },
                      })
                    }
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Image URL</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) =>
                      setState({
                        ...state,
                        newTVShow: {
                          ...state.newTVShow,
                          imageURL: e.target.value,
                        },
                      })
                    }
                  />
                </Form.Group>
              </span>
            )}
          </Form>
        )}
      </Modal.Body>
      <Modal.Footer>
        {!state.showProgress && (
          <>
            <Button
              variant="secondary"
              onClick={() => {
                setState(initialState);
                hideModal();
              }}
            >
              Cancel
            </Button>
            <Button
              variant="primary"
              onClick={async () => {
                if (!state.showCustomForm) {
                  if (state.selectedIndex === -1) {
                    alert("Please select a tv show from the search results.");
                    return;
                  }
                  const { id, poster_path } = state.searchResults[
                    state.selectedIndex
                  ];
                  const { name, overview, seasons } = await TMDB.getTVShowByID(
                    id
                  );
                  const { secure_base_url } = (await TMDB.getConfig()).images;
                  const tvshow = {
                    title: name,
                    description: overview,
                    imageURL: `${secure_base_url}w300${poster_path}`,
                  };
                  await TVSHOWS.create(tvshow);
                  setState({
                    ...state,
                    showProgress: true,
                    percentage: 0,
                  });
                  for (let i = 0; i < seasons.length; i++) {
                    const {
                      name: seasonName,
                      overview: seasonOverview,
                      season_number,
                      poster_path: seasonPoster,
                      episode_count,
                    } = seasons[i];
                    if (season_number === 0) {
                      continue;
                    }
                    await SEASONS.create({
                      tvshow: tvshow.title,
                      title: seasonName,
                      description: seasonOverview,
                      seasonNumber: season_number,
                      imageURL: `${secure_base_url}w300${seasonPoster}`,
                    });
                    for (let j = 0; j < episode_count; j++) {
                      const {
                        name: episodeName,
                        overview: episodeOverview,
                        episode_number,
                      } = await TMDB.getEpisode(id, season_number, j + 1);
                      const newEpisode = {
                        tvshow: tvshow.title,
                        seasonNumber: season_number,
                        title: episodeName,
                        description: episodeOverview,
                        episodeNumber: episode_number,
                        fileName: "",
                      };
                      await EPISODES.create(newEpisode);
                    }
                    setState({
                      ...state,
                      showProgress: true,
                      percentage: ((i + 1) / seasons.length) * 100,
                    });
                  }
                } else {
                  await TVSHOWS.create(state.newTVShow);
                }
                setState(initialState);
                await refresh("tvshows");
                await refresh("seasons", "tvshows/");
                await refresh("episodes", "tvshows/");
                hideModal();
              }}
            >
              Submit
            </Button>
          </>
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default NewTVShowModal;
