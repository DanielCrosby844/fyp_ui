import { useState, useContext, useEffect } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import { BsFillTrashFill } from "react-icons/bs";
import Api from "../../Api";

const EditImageModal = ({ show, hideModal }) => {
  const { token, images, refresh } = useContext(Context);
  const IMAGES = new Api("images", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Edit Images</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Table>
          <thead>
            <tr>
              <th>File Name</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {images.map((image, index) => {
              return (
                <tr key={index}>
                  <td>{image.fileName}</td>
                  <td>
                    <BsFillTrashFill
                      onClick={async () => {
                        await IMAGES.delete({ id: image._id.$oid });
                        await refresh("images");
                      }}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            hideModal();
          }}
        >
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditImageModal;
