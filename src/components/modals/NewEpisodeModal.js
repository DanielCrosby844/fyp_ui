import { useState, useContext } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Api from "../../Api";

const NewEpisodeModal = ({ show, hideModal, match }) => {
  const { token, tvshows, setContext } = useContext(Context);
  const initialState = {
    title: "",
    description: "",
    episodeNumber: 0,
    fileName: "",
  };
  const [state, setState] = useState(initialState);
  const EPISODES = new Api("tvshows/episodes", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Add Episode</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              onChange={(e) =>
                setState({
                  ...state,
                  title: e.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              onChange={(e) =>
                setState({
                  ...state,
                  description: e.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Episode Number</Form.Label>
            <Form.Control
              type="number"
              min="1"
              onChange={(e) =>
                setState({ ...state, episodeNumber: parseInt(e.target.value) })
              }
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setState(initialState);
            hideModal();
          }}
        >
          Cancel
        </Button>
        <Button
          variant="primary"
          onClick={async () => {
            const { title } = tvshows.find(
              (tvshow) => tvshow._id.$oid === match.params.id
            );
            await EPISODES.create({
              ...state,
              tvshow: title,
              seasonNumber: parseInt(match.params.seasonNumber),
            });
            setState(initialState);
            hideModal();
            setContext({ episodes: await EPISODES.getAll() });
          }}
        >
          Submit
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default NewEpisodeModal;
