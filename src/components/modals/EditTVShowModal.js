import { useState, useContext, useEffect } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import { BsFillTrashFill } from "react-icons/bs";
import { FiEdit } from "react-icons/fi";
import Api from "../../Api";

const EditTVShowModal = ({ show, hideModal }) => {
  const { token, tvshows, seasons, episodes, refresh } = useContext(Context);
  const initialState = {
    showEditForm: false,
    originalTVShow: {},
    editedTVShow: { title: "", description: "" },
  };
  const [state, setState] = useState(initialState);
  const TVSHOWS = new Api("tvshows", token);
  const SEASONS = new Api("tvshows/seasons", token);
  const EPISODES = new Api("tvshows/episodes", token);
  useEffect(async () => {
    if (!tvshows.length || !seasons.length || !episodes.length) {
      await refresh("tvshows");
      await refresh("seasons", "tvshows/");
      await refresh("episodes", "tvshows/");
    }
  }, []);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Edit TVShows</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {state.showEditForm ? (
          <Form>
            <Form.Group>
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                value={state.editedTVShow.title}
                onChange={(e) =>
                  setState({
                    ...state,
                    editedTVShow: {
                      ...state.editedTVShow,
                      title: e.target.value,
                    },
                  })
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={state.editedTVShow.description}
                onChange={(e) =>
                  setState({
                    ...state,
                    editedTVShow: {
                      ...state.editedTVShow,
                      description: e.target.value,
                    },
                  })
                }
              />
            </Form.Group>
          </Form>
        ) : (
          <Table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Delete</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>
              {tvshows.map((tvshow, index) => {
                return (
                  <tr key={index}>
                    <td>{tvshow.title}</td>
                    <td>
                      <BsFillTrashFill
                        size={20}
                        onClick={async () => {
                          let episodesToDelete = episodes.filter(
                            (episode) => episode.tvshow === tvshow.title
                          );
                          for (let i = 0; i < episodesToDelete.length; i++) {
                            await EPISODES.delete({
                              id: episodesToDelete[i]._id.$oid,
                            });
                          }
                          let seasonsToDelete = seasons.filter(
                            (season) => season.tvshow === tvshow.title
                          );
                          for (let i = 0; i < seasonsToDelete.length; i++) {
                            await SEASONS.delete({
                              id: seasonsToDelete[i]._id.$oid,
                            });
                          }
                          await TVSHOWS.delete({ id: tvshow._id.$oid });
                          await refresh("seasons", "tvshows/");
                          await refresh("episodes", "tvshows/");
                          await refresh("tvshows");
                        }}
                      />
                    </td>
                    <td>
                      <FiEdit
                        size={20}
                        onClick={() => {
                          setState({
                            ...state,
                            showEditForm: true,
                            originalTVShow: tvshow,
                            editedTVShow: tvshow,
                          });
                        }}
                      />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setState(initialState);
            hideModal();
          }}
        >
          Cancel
        </Button>
        {state.showEditForm && (
          <Button
            onClick={async () => {
              let title = state.originalTVShow.title;
              let episodesToUpdate = episodes.filter(
                (episode) => episode.tvshow === title
              );
              for (let i = 0; i < episodesToUpdate.length; i++) {
                await EPISODES.update({
                  ...episodesToUpdate[i],
                  tvshow: state.editedTVShow.title,
                  id: episodesToUpdate[i]._id.$oid,
                });
              }
              let seasonsToUpdate = seasons.filter(
                (season) => season.tvshow === title
              );
              for (let i = 0; i < seasonsToUpdate.length; i++) {
                await SEASONS.update({
                  ...seasonsToUpdate[i],
                  tvshow: state.editedTVShow.title,
                  id: seasonsToUpdate[i]._id.$oid,
                });
              }
              await TVSHOWS.update({
                ...state.editedTVShow,
                id: state.editedTVShow._id.$oid,
              });
              await refresh("episodes", "tvshows/");
              await refresh("seasons", "tvshows/");
              await refresh("tvshows");
              setState(initialState);
              hideModal();
            }}
          >
            Submit
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default EditTVShowModal;
