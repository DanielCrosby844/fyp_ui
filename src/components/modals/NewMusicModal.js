import { useState, useContext } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ProgressBar from "react-bootstrap/ProgressBar";
import Api from "../../Api";

const NewMusicModal = ({ show, hideModal }) => {
  const { token, setContext } = useContext(Context);
  const initialState = {
    title: "",
    artist: "",
    fileName: "",
    showProgress: false,
    percentage: 0,
  };
  const [state, setState] = useState(initialState);
  const MUSIC = new Api("music", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Add Music</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {state.showProgress ? (
          <ProgressBar
            animated
            now={state.percentage}
            label={`${Math.trunc(state.percentage)}%`}
          />
        ) : (
          <Form>
            <Form.Group>
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                onChange={(e) => setState({ ...state, title: e.target.value })}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Artist</Form.Label>
              <Form.Control
                type="text"
                onChange={(e) => setState({ ...state, artist: e.target.value })}
              />
            </Form.Group>
            <Form.Group>
              <Form.Control type="file" name="file" id="music-file" />
            </Form.Group>
          </Form>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setState(initialState);
            hideModal();
          }}
        >
          Cancel
        </Button>
        <Button
          onClick={async () => {
            const files = document.getElementById("music-file").files;
            if (files.length === 0) {
              alert("Please upload a file");
              return;
            }
            await MUSIC.uploadFile(files[0], (percentage) => {
              setState({
                ...state,
                showProgress: true,
                percentage: percentage,
              });
            });
            await MUSIC.create({ ...state, fileName: files[0].name });
            setState(initialState);
            hideModal();
            setContext({ music: await MUSIC.getAll() });
          }}
        >
          Submit
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default NewMusicModal;
