import { useState, useContext } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import ListGroup from "react-bootstrap/ListGroup";
import TMDB from "../../TMDB";
import Button from "react-bootstrap/Button";
import MediaCard from "../MediaCard";
import ProgressBar from "react-bootstrap/ProgressBar";
import Api from "../../Api";

const NewMovieModal = ({ show, hideModal }) => {
  const { token, setContext } = useContext(Context);
  const initialState = {
    searchResults: [],
    selectedIndex: -1,
    showCustomForm: false,
    showUpload: false,
    showProgress: false,
    newMovie: {},
    percentage: 1,
  };
  const [state, setState] = useState(initialState);
  const MOVIES = new Api("movies", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Add Movie</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {state.showUpload ? (
          <div>
            {state.showProgress ? (
              <ProgressBar
                animated
                now={state.percentage}
                label={`${Math.trunc(state.percentage)}%`}
              />
            ) : (
              <div>
                <Form style={{ marginBottom: "5%" }}>
                  <Form.Control type="file" name="file" id="movie-file" />
                </Form>
                <MediaCard media={state.newMovie} />
              </div>
            )}
          </div>
        ) : (
          <Form>
            <Form.Group>
              <Form.Control
                placeholder="Search..."
                type="search"
                disabled={state.showCustomForm}
                onChange={async (e) => {
                  setState({
                    ...state,
                    searchResults:
                      e.target.value === ""
                        ? []
                        : (await TMDB.searchMovies(e.target.value)).results,
                  });
                }}
              />
            </Form.Group>
            <ListGroup className="search-results">
              {state.searchResults.map((result, index) => {
                return (
                  <ListGroup.Item
                    key={index}
                    active={index === state.selectedIndex}
                    onClick={() => setState({ ...state, selectedIndex: index })}
                  >
                    {result.title}
                  </ListGroup.Item>
                );
              })}
            </ListGroup>
            <Form.Group>
              <Form.Check
                label="Custom"
                onChange={(e) => {
                  const checked = e.target.checked;
                  setState({
                    ...state,
                    searchResults: [],
                    showCustomForm: checked,
                  });
                }}
              />
            </Form.Group>
            {state.showCustomForm && (
              <span>
                <Form.Group>
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) =>
                      setState({
                        ...state,
                        newMovie: { ...state.newMovie, title: e.target.value },
                      })
                    }
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    onChange={(e) =>
                      setState({
                        ...state,
                        newMovie: {
                          ...state.newMovie,
                          description: e.target.value,
                        },
                      })
                    }
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Image URL</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) =>
                      setState({
                        ...state,
                        newMovie: {
                          ...state.newMovie,
                          imageURL: e.target.value,
                        },
                      })
                    }
                  />
                </Form.Group>
              </span>
            )}
          </Form>
        )}
      </Modal.Body>
      <Modal.Footer>
        {!state.showProgress && (
          <>
            <Button
              variant="secondary"
              onClick={() => {
                setState(initialState);
                hideModal();
              }}
            >
              Cancel
            </Button>
            {state.showUpload ? (
              <Button
                onClick={async () => {
                  const files = document.getElementById("movie-file").files;
                  if (files.length === 0) {
                    alert("Please upload a file");
                    return;
                  }
                  state.newMovie.fileName = files[0].name;
                  await MOVIES.uploadFile(files[0], (percentage) => {
                    setState({
                      ...state,
                      percentage: percentage,
                      showProgress: true,
                    });
                  });
                  await MOVIES.create(state.newMovie);
                  setState(initialState);
                  hideModal();
                  setContext({ movies: await MOVIES.getAll() });
                }}
              >
                Submit
              </Button>
            ) : (
              <Button
                onClick={async () => {
                  if (state.showCustomForm) {
                    setState({ ...state, showUpload: true });
                    return;
                  }
                  if (state.selectedIndex === -1) {
                    alert("Please select a movie from the search results.");
                    return;
                  }
                  const { title, overview, poster_path } = state.searchResults[
                    state.selectedIndex
                  ];
                  const tmdbConfig = (await TMDB.getConfig()).images;
                  setState({
                    ...state,
                    showUpload: true,
                    newMovie: {
                      title: title,
                      description: overview,
                      imageURL: `${tmdbConfig.secure_base_url}w300${poster_path}`,
                    },
                  });
                }}
              >
                Continue
              </Button>
            )}
          </>
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default NewMovieModal;
