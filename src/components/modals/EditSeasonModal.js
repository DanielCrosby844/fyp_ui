import { useState, useContext, useEffect } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import { BsFillTrashFill } from "react-icons/bs";
import { FiEdit } from "react-icons/fi";
import Api from "../../Api";

const EditSeasonModal = ({ show, hideModal, tvshow }) => {
  const { token, seasons, episodes, refresh } = useContext(Context);
  const initialState = {
    showEditForm: false,
    editedSeason: { title: "", description: "" },
  };
  const [state, setState] = useState(initialState);
  const SEASONS = new Api("tvshows/seasons", token);
  const EPISODES = new Api("tvshows/episodes", token);
  useEffect(async () => {
    if (!seasons.length || !episodes.length) {
      await refresh("seasons", "tvshows/");
      await refresh("episodes", "tvshows/");
    }
  }, []);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Edit Seasons</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {state.showEditForm ? (
          <Form>
            <Form.Group>
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                value={state.editedSeason.title}
                onChange={(e) =>
                  setState({
                    ...state,
                    editedSeason: {
                      ...state.editedSeason,
                      title: e.target.value,
                    },
                  })
                }
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={state.editedSeason.description}
                onChange={(e) =>
                  setState({
                    ...state,
                    editedSeason: {
                      ...state.editedSeason,
                      description: e.target.value,
                    },
                  })
                }
              />
            </Form.Group>
          </Form>
        ) : (
          <Table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Delete</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>
              {seasons
                .filter((season) => season.tvshow === tvshow)
                .map((season, index) => {
                  return (
                    <tr key={index}>
                      <td>{season.title}</td>
                      <td>
                        <BsFillTrashFill
                          size={20}
                          onClick={async () => {
                            let episodesToDelete = episodes.filter(
                              (episode) =>
                                episode.tvshow === tvshow &&
                                episode.seasonNumber === season.seasonNumber
                            );
                            for (let i = 0; i < episodesToDelete.length; i++) {
                              await EPISODES.delete({
                                id: episodesToDelete[i]._id.$oid,
                              });
                            }
                            await SEASONS.delete({ id: season._id.$oid });
                            await refresh("seasons", "tvshows/");
                            await refresh("episodes", "tvshows/");
                          }}
                        />
                      </td>
                      <td>
                        <FiEdit
                          size={20}
                          onClick={() => {
                            setState({
                              ...state,
                              showEditForm: true,
                              editedSeason: season,
                            });
                          }}
                        />
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setState(initialState);
            hideModal();
          }}
        >
          Cancel
        </Button>
        {state.showEditForm && (
          <Button
            onClick={async () => {
              await SEASONS.update({
                ...state.editedSeason,
                id: state.editedSeason._id.$oid,
              });
              await refresh("seasons", "tvshows/");
              setState({ ...state, showEditForm: false });
            }}
          >
            Submit
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default EditSeasonModal;
