import { useState, useContext, useEffect } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Api from "../../Api";

const EditMovieModal = ({ show, hideModal, movie, history }) => {
  const { token, setContext } = useContext(Context);
  const [state, setState] = useState({
    updatedMovie: { title: "", description: "" },
  });
  useEffect(() => {
    setState({ ...state, updatedMovie: movie });
  }, [movie]);
  const MOVIES = new Api("movies", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>{movie.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              value={state.updatedMovie.title}
              onChange={(e) => {
                setState({
                  ...state,
                  updatedMovie: {
                    ...state.updatedMovie,
                    title: e.target.value,
                  },
                });
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              value={state.updatedMovie.description}
              rows={3}
              onChange={(e) => {
                setState({
                  ...state,
                  updatedMovie: {
                    ...state.updatedMovie,
                    description: e.target.value,
                  },
                });
              }}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setState({ ...state, updatedMovie: movie });
            hideModal();
          }}
        >
          Cancel
        </Button>
        <Button
          variant="danger"
          onClick={async () => {
            await MOVIES.delete({ id: state.updatedMovie._id.$oid });
            setContext({ movies: await MOVIES.getAll() });
            history.push("/movies");
          }}
        >
          Delete
        </Button>
        <Button
          variant="primary"
          onClick={async () => {
            await MOVIES.update({
              ...state.updatedMovie,
              id: state.updatedMovie._id.$oid,
            });
            hideModal();
            setContext({ movies: await MOVIES.getAll() });
          }}
        >
          Update
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditMovieModal;
