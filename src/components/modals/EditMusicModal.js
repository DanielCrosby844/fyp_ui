import { useState, useContext, useEffect } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Api from "../../Api";

const EditMusicModal = ({ show, hideModal, music }) => {
  const { token, setContext } = useContext(Context);
  const [state, setState] = useState({
    updatedMusic: {},
  });
  useEffect(() => {
    setState({ ...state, updatedMusic: music });
  }, [music]);
  const MUSIC = new Api("music", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>{music.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              value={state.updatedMusic.title}
              onChange={(e) => {
                setState({
                  ...state,
                  updatedMusic: {
                    ...state.updatedMusic,
                    title: e.target.value,
                  },
                });
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Artist</Form.Label>
            <Form.Control
              type="text"
              value={state.updatedMusic.artist}
              onChange={(e) => {
                setState({
                  ...state,
                  updatedMusic: {
                    ...state.updatedMusic,
                    artist: e.target.value,
                  },
                });
              }}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setState({ ...state, updatedMusic: music });
            hideModal();
          }}
        >
          Cancel
        </Button>
        <Button
          variant="danger"
          onClick={async () => {
            await MUSIC.delete({ id: state.updatedMusic._id.$oid });
            setContext({ music, music: await MUSIC.getAll() });
            hideModal();
          }}
        >
          Delete
        </Button>
        <Button
          variant="primary"
          onClick={async () => {
            await MUSIC.update({
              ...state.updatedMusic,
              id: state.updatedMusic._id.$oid,
            });
            hideModal();
            setContext({ music: await MUSIC.getAll() });
          }}
        >
          Update
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditMusicModal;
