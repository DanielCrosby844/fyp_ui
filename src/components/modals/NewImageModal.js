import { useState, useContext } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ProgressBar from "react-bootstrap/ProgressBar";
import Api from "../../Api";

const NewImageModal = ({ show, hideModal }) => {
  const { token, images, setContext } = useContext(Context);
  const initialState = { showProgress: false, percentage: 0 };
  const [state, setState] = useState(initialState);
  const IMAGES = new Api("images", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Add Image</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {state.showProgress ? (
          <ProgressBar
            animated
            now={state.percentage}
            label={`${Math.trunc(state.percentage)}%`}
          />
        ) : (
          <Form>
            <Form.Group>
              <Form.Control type="file" name="file" id="image-file" />
            </Form.Group>
          </Form>
        )}
      </Modal.Body>
      <Modal.Footer>
        {!state.showProgress && (
          <>
            <Button
              onClick={() => {
                setState(initialState);
                hideModal();
              }}
            >
              Cancel
            </Button>
            <Button
              onClick={async () => {
                const files = document.getElementById("image-file").files;
                if (files.length === 0) {
                  alert("Please upload a file");
                  return;
                }
                await IMAGES.uploadFile(files[0], (percentage) => {
                  setState({
                    ...state,
                    showProgress: true,
                    percentage: percentage,
                  });
                });
                await IMAGES.create({ fileName: files[0].name });
                setContext({ images: await IMAGES.getAll() });
                setState(initialState);
                hideModal();
              }}
            >
              Submit
            </Button>
          </>
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default NewImageModal;
