import { useState, useContext } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Api from "../../Api";

const NewSeasonModal = ({ show, hideModal, match }) => {
  const { token, tvshows, setContext } = useContext(Context);
  const initialState = {
    title: "",
    description: "",
    imageURL: "",
    seasonNumber: 0,
  };
  const [state, setState] = useState(initialState);
  const SEASONS = new Api("tvshows/seasons", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Add Season</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              onChange={(e) =>
                setState({
                  ...state,
                  title: e.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              onChange={(e) =>
                setState({
                  ...state,
                  description: e.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Image URL</Form.Label>
            <Form.Control
              type="text"
              onChange={(e) =>
                setState({
                  ...state,
                  imageURL: e.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Season Number</Form.Label>
            <Form.Control
              type="number"
              min="1"
              onChange={(e) =>
                setState({ ...state, seasonNumber: e.target.value })
              }
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            setState(initialState);
            hideModal();
          }}
        >
          Cancel
        </Button>
        <Button
          variant="primary"
          onClick={async () => {
            const { title } = tvshows.find(
              (tvshow) => tvshow._id.$oid === match.params.id
            );
            await SEASONS.create({ ...state, tvshow: title });
            hideModal();
            setContext({ seasons: await SEASONS.getAll() });
            setState(initialState);
          }}
        >
          Submit
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default NewSeasonModal;
