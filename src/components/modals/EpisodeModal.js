import { useState, useContext } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Api from "../../Api";
import MediaCard from "../MediaCard";
import ProgressBar from "react-bootstrap/ProgressBar";

const EpisodeModal = ({ show, hideModal, episode, history }) => {
  const { token, setContext } = useContext(Context);
  const EPISODES = new Api("tvshows/episodes", token);
  const initialState = {
    updatedEpisode: {},
    showEditForm: false,
    showUpload: false,
    showProgress: false,
    percentage: 0,
  };
  const [state, setState] = useState(initialState);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>{episode.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {state.showEditForm ? (
          <Form>
            {state.showUpload ? (
              <>
                {state.showProgress ? (
                  <ProgressBar
                    animated
                    now={state.percentage}
                    label={`${Math.trunc(state.percentage)}%`}
                  />
                ) : (
                  <Form.Control type="file" name="file" id="tvshow-file" />
                )}
              </>
            ) : (
              <>
                <Form.Group>
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    type="text"
                    value={state.updatedEpisode.title}
                    onChange={(e) => {
                      setState({
                        ...state,
                        updatedEpisode: {
                          ...state.updatedEpisode,
                          title: e.target.value,
                        },
                      });
                    }}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    value={state.updatedEpisode.description}
                    rows={3}
                    onChange={(e) => {
                      setState({
                        ...state,
                        updatedEpisode: {
                          ...state.updatedEpisode,
                          description: e.target.value,
                        },
                      });
                    }}
                  />
                </Form.Group>
              </>
            )}
          </Form>
        ) : (
          <div>
            <MediaCard media={episode} />
          </div>
        )}
      </Modal.Body>
      <Modal.Footer>
        {state.showEditForm ? (
          <div>
            {state.showUpload ? (
              <>
                <Button
                  onClick={async () => {
                    const files = document.getElementById("tvshow-file").files;
                    if (files.length === 0) {
                      alert("Please upload a file");
                      return;
                    }
                    await EPISODES.uploadFile(files[0], (percentage) => {
                      setState({
                        ...state,
                        showProgress: true,
                        percentage: percentage,
                      });
                    });
                    await EPISODES.update({
                      ...state.updatedEpisode,
                      fileName: files[0].name,
                    });
                    hideModal();
                    setState(initialState);
                    setContext({ episodes: await EPISODES.getAll() });
                  }}
                >
                  Submit
                </Button>
              </>
            ) : (
              <>
                <Button
                  onClick={() => setState({ ...state, showUpload: true })}
                >
                  Upload File
                </Button>
                {" or "}
                <Button
                  onClick={async () => {
                    await EPISODES.update(state.updatedEpisode);
                    hideModal();
                    setState(initialState);
                    setContext({ episodes: await EPISODES.getAll() });
                  }}
                >
                  Submit
                </Button>
              </>
            )}
          </div>
        ) : (
          <>
            <Button
              onClick={() => {
                hideModal();
                setState(initialState);
              }}
            >
              Close
            </Button>
            <Button
              onClick={() =>
                setState({
                  ...state,
                  showEditForm: true,
                  updatedEpisode: { ...episode, id: episode._id.$oid },
                })
              }
            >
              Edit
            </Button>
            <Button
              disabled={episode.fileName === ""}
              onClick={() => {
                history.push(
                  `${history.location.pathname}/${episode.episodeNumber}`
                );
              }}
            >
              Play
            </Button>
          </>
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default EpisodeModal;
