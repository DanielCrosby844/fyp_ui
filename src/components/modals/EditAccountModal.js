import { useState, useContext, useEffect } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Api from "../../Api";

const EditAccountModal = ({ show, account, hideModal, setAccounts }) => {
  const { token } = useContext(Context);
  const [state, setState] = useState({
    updatedAccount: { username: "", password: "" },
  });
  useEffect(() => {
    setState({
      ...state,
      updatedAccount: {
        ...state.updatedAccount,
        username: account.username || "",
      },
    });
  }, [account]);
  const ACCOUNTS = new Api("accounts", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>{account.username}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>New username</Form.Label>
            <Form.Control
              type="text"
              value={state.updatedAccount.username}
              onChange={(e) => {
                setState({
                  ...state,
                  updatedAccount: {
                    ...state.updatedAccount,
                    username: e.target.value,
                  },
                });
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>New password</Form.Label>
            <Form.Control
              type="password"
              value={state.updatedAccount.password}
              onChange={(e) => {
                setState({
                  ...state,
                  updatedAccount: {
                    ...state.updatedAccount,
                    password: e.target.value,
                  },
                });
              }}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            hideModal();
          }}
        >
          Cancel
        </Button>
        <Button variant="danger" onClick={async ()=>{
            await ACCOUNTS.delete({id: account.id})
            setAccounts(await ACCOUNTS.getAll());
        }}>Delete</Button>
        <Button
          onClick={async () => {
            if (state.updatedAccount.password === "") {
              alert("Please exter a new password");
              return;
            }
            await ACCOUNTS.update({
              ...state.updatedAccount,
              id: account.id,
            });
            setAccounts(await ACCOUNTS.getAll());
          }}
        >
          Submit
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default EditAccountModal;
