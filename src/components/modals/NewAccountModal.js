import { useState, useContext } from "react";
import Context from "../../Context";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Api from "../../Api";

const NewAccountModal = ({ show, setAccounts }) => {
  const { token, setContext } = useContext(Context);
  const initialState = {
    username: "",
    password: "",
  };
  const [state, setState] = useState(initialState);
  const ACCOUNTS = new Api("accounts", token);
  return (
    <Modal show={show} backdrop="static">
      <Modal.Header>
        <Modal.Title>Add Account</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              onChange={(e) => setState({ ...state, username: e.target.value })}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              onChange={(e) => setState({ ...state, password: e.target.value })}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={async () => await setAccounts(await ACCOUNTS.getAll())}
        >
          Cancel
        </Button>
        <Button
          onClick={async () => {
            await ACCOUNTS.create(state);
            await setAccounts(await ACCOUNTS.getAll());
          }}
        >
          Submit
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default NewAccountModal;
