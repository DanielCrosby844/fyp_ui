import Navbar from "react-bootstrap/Navbar";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";

const TopNav = ({ onSearch, onAdd }) => {
  return (
    <Navbar sticky="top" id="top-nav">
      <FormControl
        type="text"
        placeholder="Search"
        id="nav-search"
        onChange={(e) => onSearch(e)}
      />
      <Button
        variant="outline-primary"
        className="ml-auto primary-button"
        onClick={onAdd}
      >
        Add
      </Button>
    </Navbar>
  );
};

export default TopNav;
