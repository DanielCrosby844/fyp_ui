import Card from "react-bootstrap/Card";

const MediaCard = ({ media }) => {
  return (
    <div className="media-details">
      <img src={media.imageURL} alt="poster" className="movie-poster" />
      <Card className="media-card">
        <Card.Body>
          <Card.Title>{media.title}</Card.Title>
          <Card.Text>
            {media.description !== ""
              ? media.description
              : "No Description available"}
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
};

export default MediaCard;
