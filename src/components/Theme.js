import { useContext, useEffect } from "react";
import Context from "../Context";

const Theme = () => {
  const { theme } = useContext(Context);
  useEffect(() => {
    const setLightTheme = () => {
      document.documentElement.style.setProperty("--primary", "#dfdfdf");
      document.documentElement.style.setProperty("--secondary", "#111");
      document.documentElement.style.setProperty("--tertiary", "#fff");
    };
    const setDarkTheme = () => {
      document.documentElement.style.setProperty("--primary", "#111");
      document.documentElement.style.setProperty("--secondary", "#fff");
      document.documentElement.style.setProperty("--tertiary", "#151515");
    };
    theme === "light" ? setLightTheme() : setDarkTheme();
  }, [theme]);
  return null;
};

export default Theme;
