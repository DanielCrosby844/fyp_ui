import { useContext, useEffect, useState } from "react";
import Context from "../Context";
import { Link } from "react-router-dom";
import { BiCameraMovie, BiMusic } from "react-icons/bi";
import { CgScreen } from "react-icons/cg";
import { BsImages } from "react-icons/bs";
import { FiSettings } from "react-icons/fi";
import { AiOutlineHome } from "react-icons/ai";

const SideLink = ({ to, id = "", label, state, setState, icon }) => {
  return (
    <Link
      to={to}
      id={id}
      className={`sidelink ${label === state.active ? "sidelink-active" : ""}`}
      onClick={() => setState({ ...state, active: label })}
    >
      {icon}
      <span className="sidebar-text">{label}</span>
    </Link>
  );
};

const Sidebar = () => {
  const { token } = useContext(Context);
  const [state, setState] = useState({
    maximised: false,
    active: "",
  });
  useEffect(() => {
    if ((state.maximised && window.innerWidth <= 500) || !token) {
      return;
    }
    const changeTextDisplay = () => {
      const elements = document.getElementsByClassName("sidebar-text");
      for (let { style } of elements) {
        style.display = state.maximised ? "inline" : "none";
      }
    };
    setTimeout(() => changeTextDisplay(), state.maximised ? 500 : 0);
    const width = state.maximised ? "200px" : "75px";
    const { style } = document.getElementById("sidebar");
    style.width = width;
    style.minWidth = width;
  }, [state.maximised]);
  useEffect(() => {
    const resize = () => {
      if (window.innerWidth <= 500) {
        setState({ ...state, maximised: false });
      }
    };
    window.addEventListener("resize", resize);
    return () => window.removeEventListener("resize", resize);
  }, []);
  if (!token) {
    return null;
  }
  const sideLinks = [
    { to: "/", label: "Home", icon: <AiOutlineHome size={20} /> },
    { to: "/movies", label: "Movies", icon: <BiCameraMovie size={20} /> },
    { to: "/tvshows", label: "TVShows", icon: <CgScreen size={20} /> },
    { to: "/music", label: "Music", icon: <BiMusic size={20} /> },
    { to: "/images", label: "Images", icon: <BsImages size={20} /> },
    {
      to: "/settings",
      id: "settings-link",
      label: "Settings",
      icon: <FiSettings size={20} />,
    },
  ];
  return (
    <div id="sidebar">
      {state.maximised ? (
        <div id="sidebar-minimise" >
          <span
            id="sidebar-minimise-icon"
            onClick={() => setState({ ...state, maximised: false })}
          >
            x
          </span>
        </div>
      ) : (
        <div
          id="sidebar-maximise"
          onClick={() =>
            setState({ ...state, maximised: window.innerWidth > 500 })
          }
        >
          ☰
        </div>
      )}
      {sideLinks.map(({ to, label, icon, id }, index) => {
        return (
          <SideLink
            key={index}
            to={to}
            label={label}
            state={state}
            setState={setState}
            icon={icon}
            id={id}
          />
        );
      })}
    </div>
  );
};

export default Sidebar;
